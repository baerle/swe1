package Übung3;

public class Stopwatch {
    private final long start;

    public Stopwatch() {
        start = System.nanoTime();
    }

    public long elapsedNanos() {
        return System.nanoTime() - start;
    }

    public double elapsedSeconds() {
        return nanosInSeconds(elapsedNanos());
    }

    public double nanosInSeconds(long nanos) {
        return nanos / 1_000_000_000.0;
    }

    public String toString() {
        long nanos = elapsedNanos();
        return String.format("%,15d ns = %10f s", nanos, nanosInSeconds(nanos));
    }
}
