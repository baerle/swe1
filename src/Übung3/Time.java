package Übung3;

public abstract class Time implements Comparable<Time>, Iterable<Time> {
    protected Time repeatingEndDate;
    protected boolean[] repeatingCyclus;
    protected int actualIteration = 0;
    protected Time actualDate;

    public abstract int getDay();

    public abstract int getMonth();

    public abstract int getYear();

    public abstract int getHour();

    public abstract int getMinute();

    public Time getRepeatingEndDate() {
        return repeatingEndDate;
    }

    public boolean[] getRepeatingCyclus() {
        return repeatingCyclus;
    }

    public abstract boolean isBefore(Time t);

    public boolean isEqual(Time t) {
        return t.getYear() == this.getYear() && t.getMonth() == this.getMonth() && t.getDay() == this.getDay() && t.getHour() == this.getHour() && t.getMinute() == this.getMinute();
    }

    public int compareTo(Time time) {
        if (isBefore(time))
            return 1;
        return -1;
    }

    @Override
    public String toString() {
        return this.getDay() + ";" + this.getMonth() + ";" + this.getYear() + ";" + this.getHour() + ";" + this.getMinute();
    }

    public String toStringFormatted() {
        String hour = String.format("%02d", this.getHour());
        String minute = String.format("%02d", this.getMinute());
        String day = String.format("%02d", this.getDay());
        String month = String.format("%02d", this.getMonth());
        String year = String.format("%02d", this.getYear());
        String repetition = "";
        String repeatingCyclus = "";
        if (this.repeatingCyclus[0])
            repeatingCyclus = "daily";
        else if (this.repeatingCyclus[1])
            repeatingCyclus = "weekly";
        else if (this.repeatingCyclus[2])
            repeatingCyclus = "monthly";
        else if (this.repeatingCyclus[3])
            repeatingCyclus = "yearly";
        if (!repeatingCyclus.equals("")) {
            repetition = " with " + repeatingCyclus + " repetition";
            if (repeatingEndDate != null)
                repetition += " ending at " + repeatingEndDate.toStringOnlyDateFormatted();
        }
        return hour + ":" + minute + " " + day + "." + month + "." + year + repetition;
    }

    public String toStringOnlyDateFormatted() {
        String day = String.format("%02d", this.getDay());
        String month = String.format("%02d", this.getMonth());
        String year = String.format("%02d", this.getYear());

        return day + "." + month + "." + year;
    }

    public boolean equals(Time obj) {
        return obj.getHour() == this.getHour() && obj.getMinute() == this.getMinute() && obj.getDay() == this.getDay() && obj.getMonth() == this.getMonth() && obj.getYear() == this.getYear();
    }

    public void resetIterator() {
        this.actualIteration = 0;
    }
}