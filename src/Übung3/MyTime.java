package Übung3;

import java.util.Iterator;

public class MyTime extends Time {
    private final int day;
    private final int month;
    private final int year;
    private final int hour;
    private final int minute;

    public MyTime(int day, int month, int year, int hour, int minute) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.hour = hour;
        this.minute = minute;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public boolean isBefore(Time t) {
        if (t.getYear() != this.getYear())
            return t.getYear() < this.getYear();
        if (t.getMonth() != this.getMonth())
            return t.getMonth() < this.getMonth();
        if (t.getDay() != this.getDay())
            return t.getDay() < this.getDay();
        if (t.getHour() != this.getHour())
            return t.getHour() < this.getHour();
        if (t.getMinute() != this.getMinute())
            return t.getMinute() < this.getMinute();
        return false;
    }

    public boolean isEqual(Time t) {
        return t.getYear() == this.year && t.getMonth() == this.month && t.getDay() == this.day && t.getHour() == this.hour && t.getMinute() == this.minute;
    }

    @Override
    public String toString() {
        return this.day + "." + this.month + "." + this.year + " " + this.hour + ":" + this.minute;
    }

    @Override
    public Iterator<Time> iterator() {
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public Time next() {
                return null;
            }
        };
    }
}
