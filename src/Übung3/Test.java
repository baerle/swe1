package Übung3;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        ArrayList<Double> time = new ArrayList<>(), timeImproved = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Stopwatch eins = new Stopwatch();
            Event swe1Vorlesung = new Event(new MyTime(20, 4, 2020, 11, 30), "SWE1 Vorlesung", 90);
            Event swe1Übung = new Event(new MyTime(20, 4, 2020, 9, 45), "SWE1 Übung", 90);

            if (swe1Vorlesung.isEqual(swe1Übung)) {
                System.out.println("Event function isEqual returning false result!");
            } else {
                System.out.println("All is working fine");
            }

            if (swe1Vorlesung.getDatetime().toString().equals("20.4.2020 11:30")) {
                System.out.println("MyTime function toString is working fine");
            } else {
                System.out.println("MyTime function toString has an error!");
            }

            if (swe1Vorlesung.getDatetime().isBefore(swe1Übung.getDatetime())) {
                System.out.println("false");
            } else {
                System.out.println("true");
            }

            time.add(eins.nanosInSeconds(eins.elapsedNanos()));
            System.out.println(eins.toString());

            Stopwatch zwei = new Stopwatch();
            Event swe1VorlesungI = new Event(new MyTimeImproved(20, 4, 2020, 11, 30), "SWE1 Vorlesung", 90);
            Event swe1ÜbungI = new Event(new MyTimeImproved(20, 4, 2020, 9, 45), "SWE1 Übung", 90);

            if (swe1VorlesungI.isEqual(swe1ÜbungI)) {
                System.out.println("Event function isEqual returning false result!");
            } else {
                System.out.println("All is working fine");
            }

            if (swe1VorlesungI.getDatetime().toString().equals("20.4.2020 11:30")) {
                System.out.println("MyTimeImproved function toString is working fine");
            } else {
                System.out.println("MyTimeImproved function toString has an error!");
            }

            if (swe1VorlesungI.getDatetime().isBefore(swe1ÜbungI.getDatetime())) {
                System.out.println("false");
            } else {
                System.out.println("true");
            }

            timeImproved.add(zwei.nanosInSeconds(zwei.elapsedNanos()));
            System.out.println(zwei.toString());
        }
        double durchschnittTime = 0;
        for (double etime : time) {
            durchschnittTime += etime;
        }
        System.out.println("Time: " + durchschnittTime / time.size());

        durchschnittTime = 0;
        for (double etime : timeImproved) {
            durchschnittTime += etime;
        }
        System.out.println("TimeImproved: " + durchschnittTime / time.size());
    }
}
