package Übung3;

import Übung4.TimeHelper;

import java.security.InvalidParameterException;
import java.util.Iterator;

public class Event implements Comparable<Event>, Iterable<Event> {
    protected final Time datetime;
    protected final String description;
    protected final long duration;

    public Event(Time datetime, String description, long duration) {
        this.datetime = datetime;
        this.description = description;
        if (duration <= 600)
            this.duration = duration;
        else throw new InvalidParameterException("Too long duration");
    }

    public Time getDatetime() {
        return datetime;
    }

    public String getDescription() {
        return description;
    }

    public long getDuration() {
        return duration;
    }

    public Time getEndTime() {
        TimeHelper timeHelper = new TimeHelper(this.datetime);
        timeHelper.addMinutes(this.duration);
        return timeHelper.getTime();
    }

    public boolean isEqual(Event e) {
        return (this.datetime.isEqual(e.getDatetime()) && this.description.equals(e.getDescription()) && this.duration == e.getDuration());
    }

    @Override
    public String toString() {
        return "| " + this.datetime.toStringFormatted() + " | " + this.description + " | " + this.getEndTime().toStringFormatted() + " |";
    }

    public int compareTo(Event event) {
        if (isEqual(event)) return 0;
        return this.datetime.compareTo(event.getDatetime());
    }

    /**
     * asc == true means asc
     * asc == false means desc
     *
     * @param o   Event
     * @param asc boolean
     * @return int
     */
    public int compareTo(Event o, boolean asc) {
        if (asc)
            return compareTo(o);
        return -compareTo(o);
    }

    public Iterator<Event> iterator() {
        return new Iterator<>() {
            private final Iterator<Time> time = Event.this.datetime.iterator();

            public boolean hasNext() {
                return time.hasNext();
            }

            public Event next() {
                return new Event(time.next(), Event.this.description, Event.this.duration);
            }
        };
    }

    public void resetIterator() {
        this.datetime.resetIterator();
    }
}
