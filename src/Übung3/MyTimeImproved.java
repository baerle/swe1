package Übung3;

import Übung4.TimeHelper;

import java.security.InvalidParameterException;
import java.util.Iterator;

public class MyTimeImproved extends Time {
    private final int[] datetime = new int[5];

    public MyTimeImproved(int day, int month, int year, int hour, int minute, boolean[] repeatingCyclus, Time repeatingEndDate) {
        if (day <= 31) this.datetime[2] = day;
        else throw new InvalidParameterException("Invalid day");
        if (month <= 12) this.datetime[1] = month;
        else throw new InvalidParameterException("Invalid month");
        this.datetime[0] = year;
        if (hour < 24) this.datetime[3] = hour;
        else throw new InvalidParameterException("Invalid hour");
        if (minute < 60) this.datetime[4] = minute;
        else throw new InvalidParameterException("Invalid minute");
        this.repeatingCyclus = repeatingCyclus;
        this.repeatingEndDate = repeatingEndDate;
    }

    public MyTimeImproved(int day, int month, int year, int hour, int minute, boolean[] repeatingCyclus) {
        this(day, month, year, hour, minute, repeatingCyclus, null);
    }

    public MyTimeImproved(int day, int month, int year, int hour, int minute) {
        this(day, month, year, hour, minute, new boolean[]{false, false, false, false},
                new MyTimeImproved(day, month, year, hour, minute, new boolean[]{false, false, false, false}, null)
        );
    }

    public MyTimeImproved(Time time) {
        this(time.getDay(), time.getMonth(), time.getYear(), time.getHour(), time.getMinute(), time.getRepeatingCyclus(), time.getRepeatingEndDate());
    }

    public int getDay() {
        return this.datetime[2];
    }

    public int getMonth() {
        return this.datetime[1];
    }

    public int getYear() {
        return this.datetime[0];
    }

    public int getHour() {
        return this.datetime[3];
    }

    public int getMinute() {
        return this.datetime[4];
    }

    public boolean isBefore(Time t) {
        if (t.getClass().getSimpleName().equals("MyTimeImproved"))
            return _isBefore((MyTimeImproved) t);

        if (t.getYear() != this.getYear())
            return t.getYear() < this.getYear();
        if (t.getMonth() != this.getMonth())
            return t.getMonth() < this.getMonth();
        if (t.getDay() != this.getDay())
            return t.getDay() < this.getDay();
        if (t.getHour() != this.getHour())
            return t.getHour() < this.getHour();
        if (t.getMinute() != this.getMinute())
            return t.getMinute() < this.getMinute();
        return false;
    }

    private boolean _isBefore(MyTimeImproved t) {
        for (int i = 0; i < datetime.length; i++)
            if (t.datetime[i] != this.datetime[i])
                return t.datetime[i] < this.datetime[i];
        return false;
    }

    @Override
    public Iterator<Time> iterator() {
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                TimeHelper timeHelper = new TimeHelper(MyTimeImproved.this);
                for (int i = 0; i < actualIteration; i++) {
                    if (MyTimeImproved.this.repeatingCyclus[0]) //daily
                        timeHelper.addDays(1);
                    if (MyTimeImproved.this.repeatingCyclus[1]) //weekly
                        timeHelper.addDays(7);
                    if (MyTimeImproved.this.repeatingCyclus[2]) //monthly
                        timeHelper.addMonths(1);
                    if (MyTimeImproved.this.repeatingCyclus[3]) //yearly
                        timeHelper.addYears(1);
                }
                MyTimeImproved.this.actualDate = timeHelper.getTime();
                if (MyTimeImproved.this.repeatingEndDate == null || MyTimeImproved.this.repeatingEndDate.isBefore(MyTimeImproved.this.actualDate))  //actualDate < repeatingEndDate
                    return true;
                else {
                    resetIterator();
                    return false;
                }
            }

            @Override
            public Time next() {
                MyTimeImproved.this.actualIteration++;
                return MyTimeImproved.this.actualDate;
            }
        };
    }
}
