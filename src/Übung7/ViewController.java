package Übung7;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class ViewController {
    private final CalculatorView view;
    private final CalculatorController control;
    private final TextField text;
    private boolean showingResult;
    private StringBuilder textBuilder;

    public ViewController(CalculatorView view, CalculatorController control) {
        this.view = view;
        this.control = control;
        this.text = this.view.getCalculation();
        registerListener();

        this.showingResult = false;
        this.textBuilder = new StringBuilder();
    }

    private void registerListener() {
        for (Button btn : this.view.getNumbers()) {
            btn.setOnMouseClicked(e -> numberClicked(btn.getText()));
        }
        for (Button btn : this.view.getFunctions()) {
            btn.setOnMouseClicked(e -> functionClicked(btn.getText()));
        }
    }

    private void numberClicked(String btn) {
        if (this.showingResult) {
            resetTerm();
            this.showingResult = false;
        }
        this.control.addToTerm(btn);
        addToText(btn);
    }

    private void functionClicked(String btn) {
        switch (btn) {
            case "=" -> setResult(this.control.evaluateTerm());
            case "C" -> resetTerm();
            case "+/-" -> this.control.changeSign();
            default -> {
                if (this.showingResult) {
                    this.showingResult = false;
                }
                this.control.addToTerm(btn);
                addToText(btn);
            }
        }
    }

    private void addToText(String text) {
        this.text.setText(this.textBuilder.append(text).toString());
    }

    private void setResult(String result) {
        this.text.setText(result);
        this.showingResult = true;
    }

    private void resetTerm() {
        this.text.setText("0");
        this.textBuilder = new StringBuilder();
        this.control.resetTerm();
    }
}
