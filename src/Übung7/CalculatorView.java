package Übung7;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class CalculatorView extends VBox {
    TextField calculation;
    Button[] numbers;
    Button[] functions;
    HBox[] hboxes;

    public CalculatorView() {
        this.calculation = new TextField("0");
        this.calculation.setEditable(false);
        this.getChildren().add(this.calculation);

        this.hboxes = new HBox[4];
        for (int i = 0; i < this.hboxes.length; i++) {
            hboxes[i] = new HBox(15);
            this.getChildren().add(hboxes[i]);
        }

        this.numbers = new Button[10];
        for (int i = 0; i < this.numbers.length; i++) {
            this.numbers[i] = new Button(String.valueOf(i));
            this.numbers[i].setMinWidth(25);
            if (i > 6) {
                hboxes[0].getChildren().add(this.numbers[i]);
            } else if (i > 3) {
                hboxes[1].getChildren().add(this.numbers[i]);
            } else if (i > 0) {
                hboxes[2].getChildren().add(this.numbers[i]);
            } else {
                hboxes[3].getChildren().add(this.numbers[i]);
            }
        }

        this.functions = new Button[5];
        this.functions[0] = new Button("C");
        this.functions[1] = new Button("+");
        this.functions[2] = new Button("-");
        this.functions[3] = new Button("+/-");
        this.functions[4] = new Button("=");

        for (Button btn : this.functions) {
            btn.setMinWidth(25);
        }

        this.hboxes[0].getChildren().add(this.functions[0]);
        this.hboxes[1].getChildren().add(this.functions[1]);
        this.hboxes[2].getChildren().add(this.functions[2]);
        this.hboxes[3].getChildren().addAll(this.functions[3], this.functions[4]);

        this.numbers[0].setMinWidth(56);
    }

    public TextField getCalculation() {
        return calculation;
    }

    public Button[] getNumbers() {
        return numbers;
    }

    public Button[] getFunctions() {
        return functions;
    }

    public HBox[] getHboxes() {
        return hboxes;
    }
}
