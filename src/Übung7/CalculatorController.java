package Übung7;

import java.util.LinkedList;
import java.util.ListIterator;

public class CalculatorController {
    private final LinkedList<BigInteger> numbers;
    private final LinkedList<Character> operators;
    private boolean lastWasOperator;

    public CalculatorController() {
        this.numbers = new LinkedList<>();
        this.operators = new LinkedList<>();
        this.lastWasOperator = true;
    }

    public void addToTerm(String btn) {
        System.out.println(btn);
        char[] chars = btn.toCharArray();
        if (this.lastWasOperator) {
            this.numbers.add(new BigInteger());
            this.lastWasOperator = false;
        }
        if (!this.numbers.getLast().add(chars[0])) {
            this.operators.add(chars[0]);
            this.lastWasOperator = true;
        }
    }

    public String evaluateTerm() {
        System.out.println(this.numbers);
        System.out.println(this.operators);

        ListIterator<BigInteger> numberIt = this.numbers.listIterator();
        BigInteger aktNum = numberIt.next();
        for (char operator : this.operators) {
            switch (operator) {
                case '+' -> aktNum.add(numberIt.next());
                case '-' -> aktNum.subtract(numberIt.next());
            }
            numberIt.remove();
            this.operators.remove();
        }

        return aktNum.toString();
    }

    public void changeSign() {
        BigInteger number = this.numbers.get(0);
        number.setNegative(!number.isNegative());
    }

    public void resetTerm() {
        this.numbers.clear();
        this.operators.clear();
        this.lastWasOperator = true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        ListIterator<BigInteger> numberIt = this.numbers.listIterator();
        for (char operator : this.operators) {
            sb.append(numberIt.next()).append(operator);
        }
        return sb.toString();
    }
}
