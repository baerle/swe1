package Übung7;

import java.util.LinkedList;

public class BigInteger implements Comparable<BigInteger> {
    private boolean isNegative;
    private LinkedList<Integer> value;

    public BigInteger() {
        this.isNegative = false;
        this.value = new LinkedList<>();
    }

    public BigInteger(int number, boolean isNegative) {
        this.isNegative = isNegative;
        this.value = new LinkedList<>();
        if (number < 10 && number >= 0) {
            this.value.add(number);
        } else {
            char[] num = Integer.toString(number).toCharArray();
            for (char c : num) {
                this.add(getNumber(c));
            }
        }
    }

    /**
     * add number to first index position
     *
     * @param number to be added to intern LinkedList
     */
    public void add(int number) {
        if (number < 10 && number >= 0) {
            this.value.addFirst(number);//passt, weil erst Zehner, dann Einer
        } else throw new NumberFormatException("too big number");
    }

    public boolean add(char c) {
        if (Character.isDigit(c)) {
            add(getNumber(c));
        } else {
            if (this.value.size() == 0 && c == '-') {
                this.isNegative = true;
            } else return false;
        }
        return true;
    }

    /**
     * calculate add method
     *
     * @param number BigInteger to be added to this number
     */
    public void add(BigInteger number) {
        if (checkForNegation(number)) return;
        int overflow = 0;
        resizeBothToSameLength(number);
        for (int i = 0; i < this.value.size(); i++) {
            int result = this.value.get(i) + number.getValue().get(i) + overflow;
            overflow = result / 10;
            this.value.set(i, result % 10);
        }
        if (overflow > 0) {
            this.value.add(overflow);
        }
    }

    private void resizeBothToSameLength(BigInteger number) {
        if (this.value.size() > number.getValue().size()) {
            number.resizeToLength(this.value.size());
        } else {
            this.resizeToLength(number.getValue().size());
        }
    }

    private boolean checkForNegation(BigInteger number) {
        if (number.isNegative() && this.isNegative) {   //-NUM + (-NUM)
            this.isNegative = false;
            number.setNegative(false);
            this.add(number);
            this.isNegative = true;
            return true;
        } else if (this.isNegative && !number.isNegative()) {   //-NUM + NUM
            number.subtract(this);
            this.value = number.getValue();
            this.isNegative = number.isNegative();
            return true;
        } else if (!this.isNegative && number.isNegative()) {   //NUM + (-NUM)
            number.setNegative(false);
            this.subtract(number);
            return true;
        }
        return false;
    }

    public void subtract(BigInteger number) {
        if (this.compareTo(number) > -1) {
            int overflow = 0;
            resizeBothToSameLength(number);
            for (int i = 0; i < this.value.size(); i++) {
                int result = this.value.get(i) - number.getValue().get(i) - overflow;
                if (result < 0) {
                    result += 10;
                    overflow = 1;
                } else overflow = 0;
                this.value.set(i, result);
            }
        } else {
            number.subtract(this);
            this.value = number.getValue();
            this.isNegative = true;
        }
        trim();
    }

    public void multiply(BigInteger number) {   //TODO: implement
        int overflow = 0;
        resizeBothToSameLength(number);
        for (int i = 0; i < this.value.size(); i++) {
            int result = (this.value.get(i) * number.getValue().get(i)) + overflow;
            overflow = result / 10;
        }
    }

    public void resizeToLength(int newLength) {
        int difference = newLength - this.value.size();
        for (int i = 0; i < difference; i++) {
            this.value.addLast(0);
        }
    }

    public void trim() {
        for (int i = this.value.size() - 1; i > 0; i--) {
            if (this.value.get(i) == 0) {
                this.value.remove(i);
            }
        }
    }

    public LinkedList<Integer> getValue() {
        return value;
    }

    public boolean isNegative() {
        return isNegative;
    }

    public void setNegative(boolean negative) {
        isNegative = negative;
    }

    private int getNumber(char c) {
        return c - '0';
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (isNegative) {
            sb.append('-');
        }
        for (int i = this.value.size() - 1; i >= 0; i--) {
            sb.append(this.value.get(i));
        }
        return sb.toString();
    }

    @Override
    public int compareTo(BigInteger number) {
        resizeBothToSameLength(number);
        for (int i = this.value.size() - 1; i >= 0; i--) {
            if (this.value.get(i) > number.getValue().get(i)) {
                return 1;
            } else if (this.value.get(i) < number.getValue().get(i)) {
                return -1;
            }
        }
        return 0;
    }
}
