package Übung7;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) {
        CalculatorView view = new CalculatorView();
        CalculatorController control = new CalculatorController();
        ViewController viewController = new ViewController(view, control);

        Scene primaryScene = new Scene(view);

        stage.setScene(primaryScene);
        stage.show();
    }
}
