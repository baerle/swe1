package Übung7;

public class BigIntegerTest {
    public static void main(String[] args) {
        BigInteger bI = new BigInteger(1, false);
        assertVal(bI, "1", 1);

        bI.add(5);
        assertVal(bI, "15", 2);

        BigInteger bI2 = new BigInteger(7, false);
//        System.out.println(bI2.toString());
        bI.add(bI2);
        assertVal(bI, "22", 3);

        BigInteger bI3 = new BigInteger(23, true);
        bI.add(bI3);
        assertVal(bI, "-1", 4);

        BigInteger bI4 = new BigInteger(49, true);
        bI.add(bI4);
        assertVal(bI, "-50", 5);
    }

    private static void assertVal(BigInteger bI, String result, int num) {
        if (bI.toString().equals(result)) {
            System.out.println("passed tests " + num);
        } else {
            System.out.println(bI.toString());
        }
    }
}
