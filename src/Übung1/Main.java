package Übung1;

import java.util.Arrays;

public class Main {

    public Main() {
        int[] numbers = {16, 1, 3, 1, 51, 42, 36, 12, 37, 25, 12, 27};
        int[] numbersB = {24, 25, 1, 3, 1, 12, 1, 36, 1, 12, 36, 1, 25, 12, 25};
        numbers = removeDoubles(numbers);
        if (Arrays.equals(numbers, new int[]{16, 1, 3, 51, 42, 36, 12, 37, 25, 27}))
            System.out.println("alles passt");
        else {
            System.out.println("Fehler");
            for (int number : numbers) {
                System.out.println(number);
            }
        }

        System.out.println("-------------------------------------");

        numbersB = removeButFirstAndLast(numbersB);
        if (Arrays.equals(numbersB, new int[]{24, 25, 1, 3, 12, 36, 36, 1, 12, 25}))    //25 3 12 36 36 1 12 25

            System.out.println("alles passt");
        else {
            System.out.println("Fehler");
            if (!Arrays.equals(getTriplesMiddleIndex(numbersB), new int[]{11, 4, 6, 8})) {
                for (int number : numbersB) {
                    System.out.println(number);
                }
            }
        }

        System.out.println("Happy numbers:");
        for (int i = 0; i < 100; i++) {
            if (isHappyNumber(i))
                System.out.println(i);
        }
    }

    public static void main(String[] args) {
        new Main();
    }

    public boolean isPrime(int n) {
        for (int i = 2; 2 * i < n; i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    public int[] removeDoubles(int[] aArray) {
        int nextIndex = 0;
        int[] indexesToRemove = new int[aArray.length];

        //initialise indexToRemove with -1
        Arrays.fill(indexesToRemove, -1);

        //find duplicates
        for (int i = 0; i < aArray.length - 1; i++) {
            for (int j = i + 1; j < aArray.length; j++) {
                if (aArray[i] == aArray[j]) {
                    indexesToRemove[nextIndex++] = j;
                }
            }
        }

        //erase duplicates
        aArray = eraseIndexesFrom(aArray, indexesToRemove);

        return aArray;
    }

    private int[] eraseIndexesFrom(int[] aArray, int[] indexesToRemove) {
        for (int i = indexesToRemove.length - 1; i >= 0; i--)
            if (indexesToRemove[i] != -1) {
                int[] bArray = new int[aArray.length - 1];
                for (int j = 0; j < aArray.length; j++)
                    if (indexesToRemove[i] > j)
                        bArray[j] = aArray[j];
                    else if (aArray.length > j + 1)
                        bArray[j] = aArray[j + 1];
                aArray = bArray;
            }
        return aArray;
    }

    public int[] removeButFirstAndLast(int[] aArray) {
        int[] triplesMiddleIndex = getTriplesMiddleIndex(aArray);

        int counter = 0;
        for (int middleIndex : triplesMiddleIndex) {       //count number of -1 in triples
            if (middleIndex == -1)
                counter++;
        }

        int[] temp = new int[triplesMiddleIndex.length - counter];    //Array cut: leave only necessary
        System.arraycopy(triplesMiddleIndex, 0, temp, 0, triplesMiddleIndex.length - counter);
        triplesMiddleIndex = temp;

        Arrays.sort(triplesMiddleIndex);

        //erase triples
        int n = aArray.length - triplesMiddleIndex.length;  //real needed length
        if (n < aArray.length) {
            return eraseIndexesFrom(aArray, triplesMiddleIndex);
        }
        return aArray;
    }

    private int[] getTriplesMiddleIndex(int[] aArray) {
        boolean found = false;
        int nextIndex = 0;
        int[] triplesMiddleIndex = new int[aArray.length];
        Arrays.fill(triplesMiddleIndex, -1);

        //find triples
        for (int i = 0; i < aArray.length; i++) {
            for (int j = i + 1; j < aArray.length; j++) {
                if (found) continue;
                for (int k = j + 1; k < aArray.length; k++) {
                    if (found) continue;
                    if (aArray[i] == aArray[j] && aArray[j] == aArray[k]) {
                        triplesMiddleIndex[nextIndex++] = j;
                        found = true;
//                        System.out.println(i + ": " + aArray[i] + "    " + j + ": " + aArray[j] + "    " + k + ": " + aArray[k]);
                    }
                }
            }
            found = false;
        }
        return triplesMiddleIndex;
    }

    public boolean isHappyNumber(int original) {
        return isHappyNumber(original, original);
    }

    private boolean isHappyNumber(int number, int original) {
        if (number != 4) {
            String num = String.valueOf(number);
            char[] nums = num.toCharArray();
            int sum = 0;
            for (int n : nums) {
                n -= '0';
                sum += n * n;
            }
            if (sum != 1) {
                if (sum != original) {
//                    System.out.println(sum + " " + original);
                    return isHappyNumber(sum, original);
                }
                return false;
            } else {
                return true;
            }
        }
        return false;
    }
}
