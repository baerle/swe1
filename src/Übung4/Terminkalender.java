package Übung4;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.stage.Stage;
import Übung3.Event;
import Übung3.MyTimeImproved;
import Übung3.Time;

import javax.naming.directory.InvalidSearchFilterException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Terminkalender extends Application {
    private final ObservableList<Event> storage;

    public Terminkalender() {
        this.storage = FXCollections.observableArrayList();
    }

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) {
        Terminkalender calendar = new Terminkalender();
        CreateEventView createEventView = CreateEventView.createEventView();
        createEventView.getSave().setOnMouseClicked(e -> calendar.addEvent(createEventView.createEvent()));
        Scene scene = new Scene(createEventView);

        stage.setScene(scene);
        stage.show();
        /*Scanner scanner = new Scanner(System.in);

        int decision = 0;
        do {
            switch (decision) {
                case 9 -> exit(0);
                case 0, 1 -> calendar.insertEvent(calendar, scanner);
                case 3 -> calendar.deleteEvent(scanner);
                default -> calendar.printAllEvents(scanner);
            }

            System.out.println("How do you want to proceed? (Please type the number)");
            decision = calendar.readInt(scanner, "1) Add another event\n2) Read all events\n3) Delete an event\n9) exit");
        } while (decision != 0);*/
    }

    public void addEvent(Event event) {
        this.storage.add(event);
    }

    private void insertEvent(Terminkalender calendar, Scanner scanner) {
        System.out.println("Please insert the following fields:");
        System.out.print("Description: ");
        String description = scanner.nextLine();
        System.out.println(" ");

        System.out.println("Time:");
        MyTimeImproved time = readTimeFromInput(scanner);
        int duration = readInt(scanner, "Duration (in minutes):", 1, 600);

        Event event = new Event(time, description, duration);
        calendar.addEvent(event);
    }

    private void printAllEvents(Scanner scanner) {
        System.out.println("In which order do you want your output?");
        int selection = readInt(scanner, "1) Unsorted\n2) Ascending sorted\n3) Descending sorted", 1, 3);
        switch (selection) {
            case 1 -> printAllEvents(false, false);  //TODO: refactor to let user decide on counter
            case 3 -> printAllEvents(true, false);
            default -> printAllEvents(true, true);
        }
    }

    public void printAllEvents(boolean sorted, boolean asc) {
        printAllEvents(sorted, asc, 5);
    }

    public void printAllEvents(boolean sorted, boolean asc, int counterForRepetition) {
        List<Event> storageExtended = new LinkedList<>(this.storage);
        int size = storageExtended.size();
        for (int j = 0; j < size; j++) {
            int i = counterForRepetition;
            Iterator<Event> eventIterator = storageExtended.get(j).iterator();
            while (eventIterator.hasNext() && i-- != 0) {
                storageExtended.add(eventIterator.next());
            }
            storageExtended.get(j).resetIterator();
        }
        storageExtended = sortEventsFrom(storageExtended, sorted, asc);
        System.out.println("------------------------------");
        for (Event event : storageExtended) {
            System.out.println(event);
        }
        System.out.println("------------------------------");
    }

    public List<Event> sortEventsFrom(List<Event> storage, boolean sorted, boolean asc) {
        if (sorted) {
            if (asc)
                storage.sort((e, o) -> e.compareTo(o, true));
            else
                storage.sort((e, o) -> e.compareTo(o, false));
        }
        return storage;
    }

    private void deleteEvent(Scanner scanner) {
        System.out.println("Please insert the time from the event you want to delete");

        try {
            removeEventAt(readTimeFromInput(scanner));
        } catch (InvalidSearchFilterException e) {
            System.out.println("No event with expected time found");
            deleteEvent(scanner);
        }
    }

    public void removeEventAt(Time time) throws InvalidSearchFilterException {
        if (!this.storage.removeIf(event -> {
            Time datetime = event.getDatetime();
            return datetime.equals(time);
        })) {
            /*System.out.println("Time: " + time.toStringOnlyDateFormatted());*/
            boolean isDeleted = false;
            for (Event event : this.storage) {
                Time lastDateTime = event.getDatetime();
                Iterator<Event> eventIterator = event.iterator();
                while (eventIterator.hasNext() && time.isBefore(lastDateTime) && !isDeleted) {    //lastDateTime < time
                    Event next = eventIterator.next();
                    if (next.getDatetime().equals(time)) {
                        if (eventIterator.hasNext())
                            this.storage.add(new Event(new MyTimeImproved(eventIterator.next().getDatetime()), event.getDescription(), event.getDuration()));
                        event = new Event(new MyTimeImproved(lastDateTime), event.getDescription(), event.getDuration());
                        isDeleted = true;
                    }
                    /*System.out.println(lastDateTime.toStringOnlyDateFormatted());*/
                    lastDateTime = next.getDatetime();
                }
                if (isDeleted) break;
            }
            if (!isDeleted)
                throw new InvalidSearchFilterException("No event with expected time found!");
        }
    }

    private int readInt(Scanner scanner, String s) {
        System.out.print(s);
        int result;
        try {
            result = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Please insert a valid number");
            return readInt(scanner, s);
        }
        return result;
    }

    private int readInt(Scanner scanner, String s, int min, int max) {
        int result = readInt(scanner, s);
        if (result > max) {
            System.out.println("You entered a too big number!");
            return readInt(scanner, s, min, max);
        }
        if (result < min) {
            System.out.println("You entered a too small number!");
            return readInt(scanner, s, min, max);
        }
        return result;
    }

    private MyTimeImproved readTimeFromInput(Scanner scanner) {
        int day = readInt(scanner, "Day:", 1, 31);
        int month = readInt(scanner, "Month:", 1, 12);
        int year = readInt(scanner, "Year:", 0, Integer.MAX_VALUE);
        int hour = readInt(scanner, "Hour:", 0, 23);
        int minute = readInt(scanner, "Minute:", 0, 59);

        return new MyTimeImproved(day, month, year, hour, minute);
    }
}
