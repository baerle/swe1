package Übung4;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CalendarController extends Application {
    private CalenderView view;
    private Stage stage;
    private Scene scene;
    private Calendar model;

    @Override
    public void start(Stage stage) {
        this.stage = stage;
        this.model = new Calendar();
        this.view = new CalenderView(this.model.getStorage());
        this.scene = new Scene(this.view);
        stage.setScene(this.scene);
        stage.show();
        registerListener();
        this.model.readFromFile();
    }

    private void registerListener() {
        CreateEventView createEventView = CreateEventView.createEventView();

        view.getCreate().setOnMouseClicked(e -> {
            Stage stage = new Stage();
            Scene newScene = new Scene(createEventView);
            stage.setScene(newScene);
            stage.show();
        });
        createEventView.getSave().setOnMouseClicked(e -> {
            this.model.addEvent(createEventView.createEvent());
            stage.setScene(this.scene);
        });
        createEventView.getAbort().setOnMouseClicked(e -> System.exit(0));
    }
}
