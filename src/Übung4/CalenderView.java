package Übung4;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

public class CalenderView extends VBox {
    private final ListView<EventImproved> listView;
    private final Button delete;
    private final Button create;
    private final Button edit;

    public CalenderView(ObservableList<EventImproved> storage) {
        listView = new ListView<>(storage);
        /*listView.setCellFactory((view, cell) -> {
            return new ListCell<EventImproved>(){
                protected void updateItem(EventImproved item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {

                    } else {

                    }
                }
            };
        });*/
        delete = new Button("delete");
        create = new Button("create");
        edit = new Button("edit");
        getChildren().addAll(listView, create, delete, edit);
        EventImproved selectedItem = listView.getFocusModel().getFocusedItem();

    }

    public Button getDelete() {
        return delete;
    }

    public Button getCreate() {
        return create;
    }

    public Button getEdit() {
        return edit;
    }
}
