package Übung4;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import Übung3.MyTimeImproved;

import java.io.*;

public class Calendar {
    public static final String EVENT_FILE = "src/Übung4/events.txt";
    private final ObservableList<EventImproved> storage = FXCollections.observableArrayList();

    public void addEvent(EventImproved event) {
        this.storage.add(event);
        writeInFile();
    }

    public ObservableList<EventImproved> getStorage() {
        return this.storage;
    }

    private void writeInFile() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(EVENT_FILE))) {
            for (EventImproved event : this.storage) {
                bw.write(event.getDatetime() + ";" + event.getDescription() + ";" + event.getDuration() + ";" + event.getPlace());
                bw.newLine();
            }
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void readFromFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(EVENT_FILE))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] lineArray = line.split("(;)");
                if (lineArray.length >= 7) {
                    int[] data = new int[lineArray.length];
                    long duration = 0;
                    for (int i = 0; i < lineArray.length; i++) {
                        switch (i) {
                            case 0, 1, 2, 3, 4 -> data[i] = Integer.parseInt(lineArray[i]);
                            case 6 -> duration = Long.parseLong(lineArray[i]);
                        }
                    }
                    this.storage.add(new EventImproved(new MyTimeImproved(data[0], data[1], data[2], data[3], data[4]), lineArray[5], duration, lineArray[7]));
                } else throw new IOException("too less data in file");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
