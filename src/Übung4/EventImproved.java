package Übung4;

import Übung3.Event;
import Übung3.MyTimeImproved;
import Übung3.Time;

import java.time.LocalDate;

public class EventImproved extends Event {
    private final String place;

    public EventImproved(LocalDate day, int hour, int minute, String description, long duration, String place) {
        super(new MyTimeImproved(day.getDayOfMonth(), day.getMonthValue(), day.getYear(), hour, minute), description, duration);
        this.place = place;
    }

    public EventImproved(Time datetime, String description, long duration, String place) {
        super(datetime, description, duration);
        this.place = place;
    }

    public String getPlace() {
        return place;
    }

    @Override
    public String toString() {
        return "| " + this.datetime.toStringFormatted() + " | " + this.description + " | " + this.place + " | " + this.getEndTime().toStringFormatted() + " |";
    }
}
