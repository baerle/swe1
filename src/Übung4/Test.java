package Übung4;

import Übung3.Event;
import Übung3.MyTimeImproved;

import javax.naming.directory.InvalidSearchFilterException;

public class Test {
    public static void main(String[] args) throws InvalidSearchFilterException {
        Terminkalender calendar = new Terminkalender();

        // Test 1
        calendar.addEvent(new Event(new MyTimeImproved(20, 4, 2020, 11, 30), "SWE1 Vorlesung", 90));
        calendar.addEvent(new Event(new MyTimeImproved(20, 4, 2020, 11, 15), "SWE1 Vorlesung", 90));
        calendar.addEvent(new Event(new MyTimeImproved(21, 4, 2020, 11, 30), "SWE1 Vorlesung", 90));
        calendar.addEvent(new Event(new MyTimeImproved(5, 5, 2020, 11, 30), "SWE1 Vorlesung", 90));
        calendar.addEvent(new Event(new MyTimeImproved(21, 4, 2020, 9, 45, new boolean[]{false, true, false, false}), "SWE1 Übung", 90));

        calendar.removeEventAt(new MyTimeImproved(5, 5, 2020, 11, 30));
        try {
            calendar.removeEventAt(new MyTimeImproved(5, 5, 2020, 11, 25));
        } catch (InvalidSearchFilterException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("the exception above is willed");
        calendar.removeEventAt(new MyTimeImproved(26, 5, 2020, 9, 45, new boolean[]{false, true, false, false}));
        calendar.removeEventAt(new MyTimeImproved(2, 6, 2020, 9, 45, new boolean[]{false, true, false, false}));

        calendar.printAllEvents(false, true);
        calendar.printAllEvents(true, true);
        calendar.printAllEvents(true, false);
    }
}
