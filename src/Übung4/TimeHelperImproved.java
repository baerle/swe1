package Übung4;

import Übung3.MyTimeImproved;
import Übung3.Time;

public class TimeHelperImproved {
    int[] time;
    int[] hold = {60, 24, -1, 12, -1};

    public TimeHelperImproved(Time time) {
        this.time = new int[5];
        this.time[0] = time.getMinute();
        this.time[1] = time.getHour();
        this.time[2] = time.getDay();
        this.time[3] = time.getMonth();
        this.time[4] = time.getYear();
    }

    public void addMinutes(int minutes) {
        genericAdd(0, minutes);
    }

    public void genericAdd(int index, int add) {
        time[index] += add;
        if (hold[index] != -1) {
            if (time[index] >= hold[index]) {
                genericAdd(index + 1, time[index] / hold[index]);
                time[index] %= hold[index];
            }
        } else {
            if (index == 2) {
                time[2] += add;
                int daysInMonth = getDaysInMonth();
                if (time[2] >= daysInMonth) {
                    addMonths(1);
                    time[2] %= daysInMonth;
                }
            } else {
                time[index] += add;
            }
        }
    }

    public void addHours(int hours) {
        genericAdd(1, hours);
    }

    public void addDays(int days) {
        genericAdd(2, days);
    }

    public void addMonths(int months) {
        genericAdd(3, months);
    }

    public void addYears(int years) {
        time[4] += years;
    }

    private int getDaysInMonth() {
        switch (time[3]) {
            case 1, 3, 5, 7, 8, 10, 12:
                return 31;
            case 4, 6, 9, 11:
                return 30;
            case 2:
                if (isLeapYear(time[4])) return 29;
                return 28;
            default:
                return -1;
        }
    }

    private boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                return year % 400 == 0;
            } else
                return true;
        } else {
            return false;
        }
    }

    public MyTimeImproved getTime() {
        return new MyTimeImproved(time[0], time[1], time[2], time[3], time[4]);
    }
}
