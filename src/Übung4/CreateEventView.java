package Übung4;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import Übung3.MyTimeImproved;
import Übung3.Time;

import java.time.LocalDate;

public class CreateEventView extends VBox {
    private final DatePicker start;
    private final DatePicker end;
    private final ChoiceBox<Integer> startTimeHour;
    private final ChoiceBox<Integer> endTimeHour;
    private final ChoiceBox<Integer> startTimeMinute;
    private final ChoiceBox<Integer> endTimeMinute;
    private final TextField description;
    private final TextField place;
    private final Button save;
    private final Button abort;

    public CreateEventView(ObservableList<Integer> hours, ObservableList<Integer> minutes, TextField description, TextField place) {
        this(new DatePicker(),
                new ChoiceBox<>(hours),
                new ChoiceBox<>(minutes),
                new DatePicker(),
                new ChoiceBox<>(hours),
                new ChoiceBox<>(minutes),
                description,
                place,
                new Button("Save"),
                new Button("Abort")
        );
    }

    public CreateEventView(DatePicker start,
                           ChoiceBox<Integer> startTimeHour,
                           ChoiceBox<Integer> startTimeMinute,
                           DatePicker end,
                           ChoiceBox<Integer> endTimeHour,
                           ChoiceBox<Integer> endTimeMinute,
                           TextField description,
                           TextField place,
                           Button save,
                           Button abort
    ) {
        this.start = start;
        this.startTimeHour = startTimeHour;
        this.startTimeMinute = startTimeMinute;
        this.end = end;
        this.endTimeHour = endTimeHour;
        this.endTimeMinute = endTimeMinute;
        this.description = description;
        this.place = place;
        this.save = save;
        this.abort = abort;

        this.getChildren().addAll(start, startTimeHour, startTimeMinute, end, endTimeHour, endTimeMinute, description, place, save, abort);
    }

    public static CreateEventView createEventView() {
        ObservableList<Integer> hours = FXCollections.observableArrayList();
        for (int i = 0; i < 24; i++) {
            hours.add(i);
        }
        ObservableList<Integer> minutes = FXCollections.observableArrayList();
        for (int i = 0; i < 60; i++) {
            minutes.add(i);
        }
        TextField description = new TextField();
        description.setPromptText("Description");
        TextField place = new TextField();
        place.setPromptText("Place");
        return new CreateEventView(hours, minutes, description, place);
    }

    public Button getSave() {
        return save;
    }

    public Button getAbort() {
        return abort;
    }

    public EventImproved createEvent() {
        LocalDate startVal = start.getValue();
        LocalDate endVal = end.getValue();
        Time startTime = new MyTimeImproved(startVal.getDayOfMonth(), startVal.getMonthValue(), startVal.getYear(), startTimeHour.getValue(), startTimeMinute.getValue());
        Time endTime = new MyTimeImproved(endVal.getDayOfMonth(), endVal.getMonthValue(), endVal.getYear(), endTimeHour.getValue(), endTimeMinute.getValue());
        long duration = new TimeHelper(endTime).substract(startTime).getMinutes();
        System.out.println(duration);
        EventImproved newEvent = new EventImproved(startVal, startTimeHour.getValue(), startTimeMinute.getValue(), description.getText(), duration, place.getText());
        System.out.println(newEvent.toString());
        return newEvent;
    }
}
