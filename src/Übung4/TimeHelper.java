package Übung4;

import Übung3.MyTimeImproved;
import Übung3.Time;

public class TimeHelper {
    Time time;
    int minute, hour, day, month, year;

    public TimeHelper(Time time) {
        this.time = time;
        this.minute = time.getMinute();
        this.hour = time.getHour();
        this.day = time.getDay();
        this.month = time.getMonth();
        this.year = time.getYear();
    }

    public void addMinutes(long minutes) {
        this.minute += minutes;
        if (this.minute >= 60) {
            addHours(minute / 60);
            this.minute %= 60;
        }
    }

    public void addHours(int hours) {
        this.hour += hours;
        if (this.hour >= 24) {
            addDays(hour / 24);
            this.hour %= 24;
        }
    }

    public void addDays(int days) {
        this.day += days;
        int daysInMonth = getDaysInMonth();
        if (this.day >= daysInMonth) {
            addMonths(1);
            this.day %= daysInMonth;
        }
    }

    public void addMonths(int months) {
        this.month += months;
        if (this.month >= 12) {
            addYears(this.month / 12);
            this.month %= 12;
        }
    }

    public void addYears(int years) {
        this.year += years;
    }

    private int getDaysInMonth() {
        switch (this.month) {
            case 1, 3, 5, 7, 8, 10, 12:
                return 31;
            case 4, 6, 9, 11:
                return 30;
            case 2:
                if (isLeapYear(this.year)) return 29;
                return 28;
            default:
                return -1;
        }
    }

    private boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                return year % 400 == 0;
            } else
                return true;
        } else {
            return false;
        }
    }

    public TimeHelper substract(Time time) {
        //TODO: implement
        return this;
    }

    public long getMinutes() {
        return getDays() * 24 * 60;
    }

    public long getDays() {
        //TODO: implement
        return 0;
    }

    public MyTimeImproved getTime() {
        return new MyTimeImproved(this.day, this.month, this.year, this.hour, this.minute, time.getRepeatingCyclus(), time.getRepeatingEndDate());
    }
}
