package Übung6;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Dictionary {
    private final List<String> list;

    public Dictionary(List<String> list) {
        this.list = new LinkedList<>(list);
    }

    public void sortReverse() {
        reverseStringsInList();
        this.list.sort(Comparator.naturalOrder());
        reverseStringsInList();
    }

    private void reverseStringsInList() {
        for (int i = 0; i < this.list.size(); i++) {
            StringBuilder sb = new StringBuilder(this.list.get(i));
            sb.reverse();
            this.list.set(i, sb.toString());
        }
    }

    public void removeDuplicates() {
        for (int i = 0; i < this.list.size() - 1; i++) {
            if (this.list.get(i).equals(this.list.get(i + 1))) {
                this.list.remove(i + 1);
            }
        }
    }

    public List<String> getList() {
        return list;
    }
}
