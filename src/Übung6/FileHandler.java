package Übung6;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class FileHandler {
    public List<String> readFromFile(File file) throws IOException {
        LinkedList<String> list = new LinkedList<>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String word;
        do {
            word = br.readLine();
            if (word != null) {
                word = word.trim();
                if (!word.equals("")) {
                    list.add(word);
                }
            }
        } while (word != null);
        br.close();
        return list;
    }

    public void writeToFile(File file, List<String> list) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        for (String element : list) {
            bw.write(element);
            bw.newLine();
        }
        bw.close();
    }
}
