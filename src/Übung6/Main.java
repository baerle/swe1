package Übung6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        FileHandler fh = new FileHandler();
        List<String> list = null;
        try {
            list = fh.readFromFile(new File(args[0]));
        } catch (FileNotFoundException e) {
            System.out.println("Wrong filename");
            System.exit(1);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        Dictionary d = new Dictionary(list);
        d.sortReverse();
        d.removeDuplicates();
        try {
            fh.writeToFile(new File(args[1]), d.getList());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
