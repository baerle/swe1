package Übung11;

import java.util.ArrayList;

public class ArrayMatching {
    public static void main(String[] args) {
        System.out.println(prePostMatch(new int[]{42, 21, 7, 5, 42, 21, 7}));   // 3
        System.out.println(prePostMatch(new int[]{42, 21, 7, 5, 5, 21}));   // 0
        System.out.println(prePostMatch(new int[]{42, 21, 3, 4, 5, 6, 7, 42, 21}));   // 2
        System.out.println(prePostMatch(new int[]{42, 21, 3, 3, 5, 42, 21, 3, 3}));   // 4
        System.out.println(prePostMatch(new int[]{42, 21, 42, 3, 4, 5, 6, 7, 42, 21, 42}));   // 3
    }

    public static int prePostMatch(int[] aArray) {
        int counter = 0;
        int akt = aArray[0];
        ArrayList<Integer> found = new ArrayList<>();
        for (int i = aArray.length - 1; i >= aArray.length / 2; i--) { //suchen von hinten
            if (akt == aArray[i]) {
                found.add(i);
            }
        }
        if (found.size() == 0) {
            return 0;
        } else {
            counter++;
        }
        for (int i = 1; i < aArray.length / 2; i++) {
            for (int index : found) {
                if (index + i <= aArray.length - 1) {
                    if (aArray[index + i] == aArray[i]) {
                        counter++;
                    } else {
                        found.remove(Integer.valueOf(index));
                    }
                }
            }
        }
        return counter;
    }
}
