package Übung11;

import java.util.Stack;

public class Brackets {
    public static void main(String[] args) {
        String str = ")a(ASdf()";
        String str2 = "<()>(<()>)";
        System.out.println(checkBrackets(str));
        System.out.println(checkBrackets2(str));
        System.out.println(checkBrackets(str2));
        System.out.println(checkBrackets2(str2));
    }

    private static boolean checkBrackets(String str) {
        int counter = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '(') {
                counter++;
            } else if (str.charAt(i) == ')') {
                counter--;
            }
            if (counter < 0) {
                return false;
            }
        }
        return counter == 0;
    }

    private static boolean checkBrackets2(String str) {
        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
                case '<' -> stack.push('>');
                case '(' -> stack.push(')');
                case '>' -> {
                    if (stack.size() == 0 || !stack.pop().equals('>')) {
                        return false;
                    }
                }
                case ')' -> {
                    if (stack.size() == 0 || !stack.pop().equals(')')) {
                        return false;
                    }
                }
            }
        }
        return stack.size() == 0;
    }
}
