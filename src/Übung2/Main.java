package Übung2;

import java.math.BigInteger;

public class Main {

    public static void main(String[] args) {
        String[] testArray = {"1", "-1", "1.345", "-1.793", "123.987", "-123.483", "12345.10265", "-12345.12345", "916491.3519562", "1.1E3", "1.1E-3", "-1.0E34", "-1.0E10", "-1.0E18", "-1.0E19", "-1.0E20"};

        Main main = new Main();
        for (String test : testArray) {
            double result = main.convert2double(test);
            System.out.println("input: "+ test + "    result: " + result);
        }
    }

    public double convert2double(String number) {
        int ergebnisBeforeComma = 0;
        int ergebnisAfterComma = 0;
        int afterCommaNumber = 10;
        boolean negative = false;
        boolean beforeComma = true;
        boolean first = true;
        boolean power = false;
        boolean powerNegative = false;
        int powerNumber = 0;
        double powerNum = 0;
        char[] numberChar = number.toCharArray();


        for (int i = 0; i < number.length(); i++) {
            if (numberChar[i] == '.') { //is comma?
                beforeComma = false;
                continue;
            }

            if (beforeComma) {  //before comma
                if (numberChar[i] == '-') { //is negative?
                    negative = true;
                    continue;
                }

                if (first) {
                    first = false;
                } else {
                    ergebnisBeforeComma *= 10;
                }

                ergebnisBeforeComma += parseCharToInt(numberChar[i]);
            } else { //after comma
                if (power) {
                    if (first) {
                        first = false;
                        if (numberChar[i] == '-') {
                            powerNegative = true;
                            continue;
                        }
                    } else {
                        powerNumber *= 10;
                    }
                    powerNumber += parseCharToInt(numberChar[i]);

                } else {
                    first = true;
                    if (numberChar[i] == 'E' || numberChar[i] == 'e') {
                        power = true;
                        continue;
                    }
                    ergebnisAfterComma += parseCharToInt(numberChar[i]);

                    ergebnisAfterComma *= 10;
                    afterCommaNumber *= 10;
                }
            }
        }

//        long _power = 1;    //TODO: convert ot BigInteger
        BigInteger _power = new BigInteger("1");
        for (int k = powerNumber; k > 0; k--) {
            _power = _power.multiply(new BigInteger("10"));
        }
        //System.out.println(_power);
        if (powerNegative) {
            powerNum = 1 / Double.parseDouble(_power.toString());
        } else {
            powerNum = Double.parseDouble(_power.toString());
        }

        if (power) {
            if (!negative)  //returning result
                return (ergebnisBeforeComma + ((double) ergebnisAfterComma / afterCommaNumber)) * powerNum;
            return -(ergebnisBeforeComma + ((double) ergebnisAfterComma / afterCommaNumber)) * powerNum;
        } else {
            if (!negative)  //returning result
                return ergebnisBeforeComma + ((double) ergebnisAfterComma / afterCommaNumber);
            return -(ergebnisBeforeComma + ((double) ergebnisAfterComma / afterCommaNumber));
        }
    }

    private int parseCharToInt(char number) {
        return number - '0';
    }
}
